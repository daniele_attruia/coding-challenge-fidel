# Coding Challenge Fidel - Daniele Attruia

You have to options:

  - Extract file dist.zip and open the index.html with a web server.
  - Follow installation steps.

### Installation

[Node.js](https://nodejs.org/) and [Git](https://git-scm.com/downloads) are required.

You need Gulp, Bower installed globally:

```sh
$ npm i -g gulp bower
```

Clone the repository and move into.

```sh
$ git clone https://bitbucket.org/daniele_attruia/coding-challenge-fidel
```

Now you need to run

```sh
$ npm install
$ bower install
```

And then

```sh
$ gulp web-server
```

### Tech

  - AngularJS v1.5.8
  - Angular-Bootstrap ( Only for the date-picker )
  - D3 v3.5.17
  - TypeScript -> The gulp task "gulp ts" compiles all TypeScript files in JavaScript ES6
  - Jade -> The gulp task gulp jade compiles all jade files in html
  - SASS -> The gulp task gulp sass compiles all scss files in css
  - RequireJS

### Comments

I used every angular component (Controllers,Directives,Services,...) as a TypeScript class and loaded with the help of a little set of classes into app_component/core folder.
For time reason I decided to use D3 but I would like to test other graphic libraries such as ChartJS and n3-charts.
