var gulp = require("gulp");
var webServer = require('gulp-webserver');
var ts = require("gulp-tsc");
var jade = require("gulp-jade");
var sass = require('gulp-sass');
var del = require("del");

var configuration = {
	base:"dattruia-"
};

// Start web server
gulp.task('web-server', ["ts", "jade", "sass", "json", "watch"], function() {
	gulp.src('dist')
		.pipe(webServer({
			livereload: true,
			directoryListing: true,
			open: "http://localhost:8000/index.html"
		}));
});

//TYPESCRIPT
gulp.task('clean-ts', function () {

  return del([
    "./dist/app_components/" + configuration.base + "**/*.js",
    "./dist/*.js"
  ]);

});

gulp.task("ts", ["clean-ts"], function () {

  var options = {
    //Set true to display error when type is not defined
    noImplicitAny: false,
    removeComments: true,
    module:'amd',
    target:'ES6',
    allowImportModule:true
  };
  var arrayTsCompile = [
    "./dev/*.ts",
    "./dev/types/**/*.d.ts"
  ];
  return gulp.src(arrayTsCompile)
    .pipe(ts(options))
    .pipe(gulp.dest("./dist/"));

});
//END TYPESCRIPT

//JADE
gulp.task("clean-jade", function() {

  return del([
	  "./dist/app_components/" + configuration.base + "**/*.html",
    "./dist/views/*.html"
  ]);

});

gulp.task("jade", ["clean-jade"], function() {

  var options = {
    pretty:true
  };

  return gulp.src(["./dev/*.jade"])
		  .pipe(jade(options))
		  .pipe(gulp.dest("./dist/"))
	  &&
	    gulp.src(["./dev/app_components/**/*.jade"])
      .pipe(jade(options))
      .pipe(gulp.dest("./dist/app_components/"));

});
//END JADE

// SASS
gulp.task('sass', function () {
	return gulp.src('./dev/styles/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./dist/styles'));
});
// END SASS

// JSON
gulp.task('json', function() {

  return gulp.src("./dev/app_components/**/locale/*.json").pipe(gulp.dest("./dist/app_components/"));

});
// END JSON

// Default compile of file
gulp.task("default", ["web-server"]);

// Watch Files For Changes
gulp.task("watch", function() {

  //TYPESCRIPT
  gulp.watch(
    ["dev/app_components/**/*.ts", "./dev/*.ts"],
    ["ts"]
  );

	//JADE
	gulp.watch(
		["dev/app_components/**/*.jade", "./dev/views/*.jade", "./dev/*.jade"],
		["jade"]
	);

	//SASS
	gulp.watch(
		["dev/styles/*.scss"],
		["sass"]
	);

  //JSON
  gulp.watch(
    ["dev/app_components/**/*.json"],
    ["json"]
  );

});
