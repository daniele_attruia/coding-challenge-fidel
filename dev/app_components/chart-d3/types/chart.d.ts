interface ITransaction {
    amount: number,
    date: number,
    dob: number,
    gender: string
}

interface ITransactionalData {
    transactions: number,
    revenues: number,
    date: string,
    timestamp: number
}

interface IFilterDate {
    dateTo:number,
    dateFrom:number,
    dateLimitTo:Date,
    dateLimitFrom:Date
}