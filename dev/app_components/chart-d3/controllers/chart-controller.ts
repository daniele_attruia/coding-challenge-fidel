import {BaseController} from"../../core/controllers/base-controller";
import {ChartTransactionsService} from "../services/transactions-service";

export class ChartController extends BaseController {

    public static $inject:Array<string> = ["ChartTransactionsService"];
    public static ngID:string = "ChartController";
    public static ngModules: Array<string>  = ['ngAnimate', 'ui.bootstrap'];

    public data: Array<ITransaction>;
    public dateFilter: IFilterDate;

    public constructor(public ChartTransactionsService:ChartTransactionsService) {
        super();

        this.data = this.ChartTransactionsService.getTransactions();

    }

}