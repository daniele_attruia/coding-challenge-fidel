import {BaseDirective} from "../../core/directives/base-directive";

export class ChartDirective extends BaseDirective {

    public static ngID: string= 'chart';
    public controller: string = 'ChartController as chartCtrl';
    public templateUrl: string = "app_components/chart-d3/views/chart.html";

    public constructor() {
        super();
    }

    public static Factory(): (...any) => ChartDirective {
        return () => {
            return new ChartDirective();
        };
    }

}