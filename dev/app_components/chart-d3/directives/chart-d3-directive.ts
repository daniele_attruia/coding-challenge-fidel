import {BaseDirective} from "../../core/directives/base-directive";
import {TransactionalService} from "../../core/services/transactional-service";

interface IChartD3Configuration {
    name:string,
    tickPadding:number
    domainDivNumber:number
}

export class ChartD3Directive extends BaseDirective {

    public static $inject: Array<string> = ["TransactionalService"];
    public static ngID: string= 'chartD3';

    public static get TRANSACTIONS():IChartD3Configuration {
        return {
            name: "transactions",
            tickPadding:10,
            domainDivNumber:1
        };
    }

    public static get REVENUES():IChartD3Configuration {
        return {
            name: "revenues",
            tickPadding: 0,
            domainDivNumber: 1.5
        };
    }

    public static get TICKS():number { return 10; }

    private transactionalData: Array<ITransactionalData>;

    public margin;
    public width;
    public height;

    public constructor(public TransactionalService:TransactionalService) {

        super();

        this.restrict = "E";

        this.scope = {
            chartD3Data: '=chartD3Data',
            dateFilter: '=dateFilter'
        };

        //setup size of line chart
        this.margin = {top: 20, right: 75, bottom: 30, left: 75};
        this.width = 1000 - this.margin.left - this.margin.right;
        this.height = 400 - this.margin.top - this.margin.bottom;

    }

    private generateChart(instanceElement, chartD3Data?:Array<ITransactionalData>) {

        if(!chartD3Data){
            chartD3Data = this.transactionalData;
        }

        /* Parse data */
        let parseDate;
        parseDate = d3.time.format("%Y-%m").parse;

        /* Set scales */
        let x = d3.time.scale()
            .range([0, this.width]);

        let yTransactions = d3.scale.linear()
            .range([this.height, 0]);

        let yRevenues = d3.scale.linear()
            .range([this.height, 0]);

        // Clear
        d3.select(instanceElement[0]).selectAll("*").remove();

        let chart = d3.select(instanceElement[0])
            .append("svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom)
            .append("g")
            .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

        /* Format data */
        chartD3Data.forEach(function(d:ITransactionalData) {
            d.date = parseDate(d.date);
        });

        //For lines/areas animation
        chart.append("clipPath")
            .attr("id", "rectClip")
            .append("rect")
            .attr("width", 0)
            .attr("height", this.height);

        this.generateXComponents(chartD3Data, chart, x);
        this.generateYComponents(chartD3Data, chart, x, yTransactions, ChartD3Directive.TRANSACTIONS);
        this.generateYComponents(chartD3Data, chart, x, yRevenues, ChartD3Directive.REVENUES);

        d3.select("#rectClip rect")
            .transition().duration(2500)
            .attr("width", this.width);

        this.generateInfoPanel(chartD3Data, chart, x, yTransactions, yRevenues);
        
    }

    public generateXComponents(chartD3Data, chart, x) {

        x.domain(d3.extent(chartD3Data, function(d:ITransactionalData) {
            return d.date;
        }));

        let xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickFormat(d3.time.format("%b"))
            .tickPadding(10)
            .innerTickSize(-this.height);

        chart.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.height + ")")
            .call(xAxis);
    }

    public generateYComponents(chartD3Data, chart, x, y, yConfig:IChartD3Configuration) {

        /* Line */
        let line = d3.svg.line()
            .x(function(d:ITransactionalData) { return x(d.date); })
            .y(function(d:ITransactionalData) { return y(d[yConfig.name]); });

        /* Area */
        let area = d3.svg.area()
            .x(function(d:ITransactionalData) { return x(d.date); })
            .y0(this.height)
            .y1(function(d:ITransactionalData) { return y(d[yConfig.name]); });

        let maxY = d3.max(chartD3Data, (d:ITransactionalData) => { return d[yConfig.name]; });
        y.domain([0, maxY+(maxY/yConfig.domainDivNumber)]);

        let yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .tickPadding(yConfig.tickPadding)
            .tickValues( this.generateTicksForYAxis(y.domain()) )
            .innerTickSize(-this.width);

        this.generateLine(chartD3Data, chart, line, yConfig.name);
        this.generateArea(chartD3Data, chart, area, yConfig.name);

        if(yConfig.name === ChartD3Directive.TRANSACTIONS.name) {
            chart.append("g")
                .attr("class", "y axis " + yConfig.name)
                .call(yAxis)
              .append("text")
                .attr("class", "label")
                .attr("transform", "rotate(-90)")
                .attr("y", "-40")
                .attr("x", "-" + (this.height + 110) / 2 + "")
                .text("TRANSACTIONS");
        } else if(yConfig.name === ChartD3Directive.REVENUES.name) {
            chart.append("g")
                .attr("class", "y axis " + yConfig.name)
                .attr("transform", "translate(" + (this.width+40) + ",0)")
                .call(yAxis)
              .append("text")
                .attr("class", "label")
                .attr("transform", "rotate(90)")
                .attr("x","" + (this.height-80)/2 + "")
                .attr("y","-15")
                .text("REVENUE (£)");
        }

    }

    public generateLine(chartD3Data, chart, line, type){
        chart.append("path")
            .attr("class", "line " + type)
            .attr("d", line(chartD3Data))
            .attr("clip-path", "url(#rectClip)");
    }

    public generateArea(chartD3Data, chart, area, type){
        chart.append("path")
            .datum(chartD3Data)
            .attr("class", "area " + type)
            .attr("d", area)
            .attr("clip-path", "url(#rectClip)");
    }

    public generateInfoPanel(chartD3Data, chart, x, y, yRevenue){

        let bisectDate = d3.bisector((d:ITransactionalData) => {
            return d.date;
        });

        let box = chart.append("g");
        box.style("display", "none")
            .append("rect")
            .attr("class", "summary-box")
            .attr("rx", "5")
            .attr("ry", "5")
            .attr("x", 10)
            .attr("y", 10);

        let titleBox = box.append("text");
        titleBox.text("")
            .attr("x", 25)
            .attr("y", 30)
            .style("fill", "white");

        box.append("rect")
            .attr("class", "box-row")
            .attr("x", 10)
            .attr("y", 40)
          .append("text");

        box.append("rect")
            .attr("class", "box-row")
            .attr("x", 10)
            .attr("y", 72);

        box.append("circle")
            .attr("class", "box-row transactions")
            .attr("transform", "translate(35,55)")
            .attr("r", 5);

        box.append("circle")
            .attr("class", "box-row revenues")
            .attr("transform", "translate(35,85)")
            .attr("x", 35)
            .attr("y", 90)
            .attr("r", 5);

        box.append("text")
            .text("Transactions ")
            .attr("x", 55)
            .attr("y", 60)
            .attr("fill", "white");

        box.append("text")
            .text("Revenues ")
            .attr("x", 55)
            .attr("y", 90)
            .attr("fill", "white");

        let rowTransactions = box.append("text");
        rowTransactions.attr("x", 160)
            .attr("y", 60)
            .attr("fill", "white");

        let rowRevenues = box.append("text");
        rowRevenues.attr("x", 135)
            .attr("y", 92)
            .attr("fill", "white");

        let focus = chart.append("g")
            .style("display", "none");

        /* Circles */
        focus.append("circle")
            .attr("class", "transactions")
            .attr("r", 5);

        focus.append("circle")
            .attr("class", "revenues")
            .attr("r", 5);

        chart.append("rect")
            .attr("width", this.width)
            .attr("height", this.height)
            .style("fill", "#e6f1f1")
            .style("pointer-events", "all")
            .style("opacity", "0.2")
            .on("mouseover", function() {
                box.style("display", null);
                focus.style("display", null);
            })
            .on("mouseout", function() {
                box.style("display", "none");
                focus.style("display", "none");
            })
            .on("mousemove", function () {
                let x0 = x.invert(d3.mouse(this)[0]+25);
                let i = bisectDate.left(chartD3Data, x0, 1);
                let d0:ITransactionalData = chartD3Data[i - 1];
                let d1:ITransactionalData = chartD3Data[i];
                let d:ITransactionalData = x0 - parseFloat(d0.date) > (d1?parseFloat(d1.date):0) - x0 ? d1 : d0;
                focus.select("circle.transactions")
                    .attr("transform", "translate(" + x(d.date) + "," + y(d.transactions) + ")");
                focus.select("circle.revenues")
                    .attr("transform", "translate(" + x(d.date) + "," + yRevenue(d.revenues) + ")");
                titleBox.text(d3.time.format("%B %Y")(<any>d.date));
                rowTransactions.text(d.transactions);
                rowRevenues.text("£"+d.revenues);
            });
    }

    public generateTicksForYAxis(yDomain:Array<any>) {
        let min = yDomain[0];
        let max = yDomain[1];
        let step = (max - min)/ ChartD3Directive.TICKS;
        return d3.range(min, max, step);
    }

    public linkFunction(scope:ng.IScope, instanceElement:ng.IAugmentedJQuery, instanceAttributes:ng.IAttributes, controller:any, transclude:ng.ITranscludeFunction):void {

        scope.$watchCollection("chartD3Data", (chartD3Data: Array<ITransaction>) => {

            this.transactionalData = this.TransactionalService.createTransactionalArray(chartD3Data);

            let dateTo = new Date(this.transactionalData[0].date);
            let dateFrom = new Date(this.transactionalData[this.transactionalData.length-1].date);
            (<any>scope).dateFilter = {
                dateTo: dateTo,
                dateFrom: dateFrom,
                dateLimitTo: dateTo,
                dateLimitFrom: dateFrom
            };

        });

        scope.$watchCollection("dateFilter", (dateFilter:IFilterDate) => {

            if( dateFilter && dateFilter.dateTo && dateFilter.dateFrom ) {

                //The date-picker adds N hours to the current selected date depending on current time zones.
                //In this way the N hours added, are set to correct value.
                let selectedDate:Date = new Date(dateFilter.dateTo);
                let dateTimeTo = new Date(selectedDate.getFullYear(), selectedDate.getMonth()).getTime();
                selectedDate = new Date(dateFilter.dateFrom);
                let dateTimeFrom:number = new Date(selectedDate.getFullYear(), selectedDate.getMonth()).getTime();

                let indexTo, indexFrom;
                this.transactionalData.forEach((transaction:ITransactionalData, index:number) => {

                    if(transaction.timestamp === dateTimeTo){
                        indexTo = index;
                    }
                    if(transaction.timestamp === dateTimeFrom){
                        indexFrom = index;
                    }

                    let monthYearDate:Date = new Date( transaction.date );

                    transaction.date = monthYearDate.getFullYear() + "-" + (monthYearDate.getMonth()+1);

                });

                let filteredTransactionalData: Array<ITransactionalData> = this.transactionalData.slice(indexTo, indexFrom+1);
                this.generateChart(instanceElement, filteredTransactionalData);
                
            }

        });
    }

    public static Factory(): (...any) => ChartD3Directive {
        return (TransactionalService) => {
            return new ChartD3Directive(TransactionalService);
        };
    }

}