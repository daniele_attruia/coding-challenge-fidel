/// <reference path='types/chart.d.ts' />

import {BaseComponent} from "../core/base-component";
import {ChartTransactionsService} from "./services/transactions-service";
import {ChartController} from "./controllers/chart-controller";
import {ChartD3Directive} from "./directives/chart-d3-directive";
import {ChartDirective} from "./directives/chart-directive";

class ChartComponent extends BaseComponent {

    public constructor() {
        super();
        this.updateConfiguration(ChartTransactionsService);
        this.updateConfiguration(ChartController);
        this.updateConfiguration(ChartDirective);
        this.updateConfiguration(ChartD3Directive);
    }

}

export var chartD3Configuration = new ChartComponent().configuration;