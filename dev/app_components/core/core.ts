import {BaseComponent} from "./base-component";
import {TransactionalService} from "./services/transactional-service";

class CoreComponent extends BaseComponent {

    constructor(){
        super();
        this.updateConfiguration(TransactionalService);
    }

}

export var coreConfiguration = new CoreComponent().configuration;