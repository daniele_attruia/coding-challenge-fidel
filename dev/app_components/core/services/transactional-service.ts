export class TransactionalService {

    public static $inject:Array<string> = [];
    public static ngID:string = "TransactionalService";
    public static ngType: string = 'factory';
    public static ngModules: Array<string>  = [];

    public constructor(){
    }

    public createTransactionalArray(chartData: Array<ITransaction>): Array<ITransactionalData> {

        let transactionalArray: Array<ITransactionalData> = [];

        let monthsTransaction = {};

        chartData.forEach((transaction:ITransaction) => {

            let monthYearDate:Date = new Date( transaction.date );

            let monthYear:string =  monthYearDate.getFullYear() + "-" + (monthYearDate.getMonth()+1);

            let monthYearKey:number = new Date(monthYearDate.getFullYear(), monthYearDate.getMonth()).getTime();

            if(!monthsTransaction[monthYearKey]){
                monthsTransaction[monthYearKey] = {
                    timestamp: monthYearKey,
                    date: monthYear,
                    transactions: 0,
                    revenues: 0,
                };
            }

            monthsTransaction[monthYearKey].transactions += 1;
            monthsTransaction[monthYearKey].revenues += Math.round(transaction.amount);

        });

        let monthsTransactionKeys = Object.keys(monthsTransaction).sort();

        monthsTransactionKeys.forEach( (key) => {
            transactionalArray.push(monthsTransaction[key]);
        });

        return transactionalArray;
    }

    public static Factory(): (...any) => TransactionalService {
        return () => {
            return new TransactionalService();
        };
    }

}