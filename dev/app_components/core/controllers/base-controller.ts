export class BaseController {

	public static ngType:string = 'controller';
	public static $inject: Array<string> = [];
    public static ngModules: Array<string>  = [];

	constructor() {
	}

}