export class BaseDirective {

    public static $inject: Array<string> = [""];
    public static ngType: string = 'directive';

    public restrict:string

    public scope: any;

    public link:(scope:ng.IScope,
                 instanceElement:ng.IAugmentedJQuery,
                 instanceAttributes:ng.IAttributes,
                 controller:any[],
                 transclude:ng.ITranscludeFunction) => void;


    public constructor(){
        // Set the link function property
        this.link = (scope:ng.IScope,
                     instanceElement:ng.IAugmentedJQuery,
                     instanceAttributes:ng.IAttributes,
                     controller:any[],
                     transclude:ng.ITranscludeFunction):void => {
            // Delegate the internal link function method
            this.linkFunction(scope, instanceElement, instanceAttributes, controller, transclude);
        };
    }

    public linkFunction(scope:ng.IScope, instanceElement:ng.IAugmentedJQuery, instanceAttributes:ng.IAttributes, controller:any, transclude:ng.ITranscludeFunction):void {

    }

}