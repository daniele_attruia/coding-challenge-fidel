export class BaseComponent {

    public configuration = {
        "ngModules" : [],
        "list" : {}
    };

    constructor(...argument) {
    }

    public updateConfiguration(module){
        if(module.ngModules) {
            this.configuration.ngModules=this.configuration.ngModules.concat(module.ngModules);
        }
        if(module.ngID && module.ngType) {
            if(module.ngType=='controller'){
                this.configuration.list[module.ngID]={callback:module,type:module.ngType}
            } else if(module.ngType=='constant'){
                this.configuration.list[module.ngID]={callback:new module(),type:module.ngType}
            } else {
                this.configuration.list[module.ngID]={callback:module.Factory(),type:module.ngType}
            }
        }
    }

}