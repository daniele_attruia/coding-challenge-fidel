/// <reference path='types/requirejs/require.d.ts' />
/// <reference path='types/d3/d3.d.ts' />

requirejs.config(<RequireConfig>{
	'waitSeconds': 0,
	'paths': {
		'angular': 'app_components/angular/angular',
		'angular-animate': 'app_components/angular-animate/angular-animate',
        'angular-bootstrap': "app_components/angular-bootstrap/ui-bootstrap-tpls",
        'd3':'app_components/d3/d3'
    },
    shim:{
    	'angular':{
    		'exports': 'angular'
    	},
        'angular-animate':['angular'],
        'd3': {
            exports: 'd3'
        },
        'angular-bootstrap' : ['angular-animate']
    }
});

var dependencies = [
    'angular-bootstrap',
    'd3'
];

require(dependencies , () => {
	require(['./loader-app-components'], () => {});
});