import angular = require('angular');

import {coreConfiguration} from "./app_components/core/core";
import {chartD3Configuration} from "./app_components/chart-d3/chart";

let applicationName = "FidelCodingChallenge";

let ngModules = [];
let newList = {};

let registerComponent = function (configuration) {
  ngModules=ngModules.concat(configuration.ngModules);
  angular.extend(newList, configuration.list);
};

registerComponent(coreConfiguration);
registerComponent(chartD3Configuration);

let app = angular.module(applicationName, ngModules);
for (let key in newList) {
	if (newList[key].type != 'config' && newList[key].type != 'run') {
      app[newList[key].type](key, newList[key].callback);
    } else {
      app[newList[key].type](newList[key].callback);
    }
}

angular.bootstrap(document, [applicationName]);
